brightnessctl (0.5.1-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Delegate placement of udev files to udev.pc (Closes: #1057229)

  [ Debian Janitor ]
  * Strip unusual field spacing from debian/control.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.
  * Set upstream metadata fields: Repository.

 -- Chris Hofstaedtler <zeha@debian.org>  Fri, 15 Dec 2023 00:43:14 +0100

brightnessctl (0.5.1-3) unstable; urgency=low

  * brightness-udev
    + Do not print the name of devices in postinst
    + Use external helper to avoid errors on certain hardware
      Closes: #970849

  * Switch to debhelper 13
  * d/control: Declare compliance with policy v4.5.1. No change required.
  * d/changelog: Update maintainer's name and email address

 -- nicoo <nicoo@debian.org>  Mon, 30 Nov 2020 16:06:43 +0100

brightnessctl (0.5.1-2) unstable; urgency=medium

  * brightness-udev: Trigger udev rules post installation.
    Thanks to Samuel Henrique for the suggestion and feedback.

  * Update authorship information

 -- nicoo <nicoo@debian.org>  Mon, 17 Feb 2020 13:59:31 +0100

brightnessctl (0.5.1-1) unstable; urgency=medium

  * New upstream release (2020-02-02)
    * Added exponential brightness scaling
    * Support for systemd/logind
    * Remove obsoleted patches (merged upstream)
    * Update debian/copyright

  * debian/control: Declare compliance with policy v4.5.0
    No change required.
  * Upgrade to dh compatibility level 12
    Switch to B-D: debhelper-compat instead of the legacy debian/compat.
  * debian/watch: Switch to using uscan's git mode
  * git-buildpackage: Enable pristine-tar support
    This ensures people building the package will get the same orig tarball.

 -- nicoo <nicoo@debian.org>  Wed, 12 Feb 2020 12:21:59 +0100

brightnessctl (0.4-2) unstable; urgency=medium

  * Apply patch for correct exit code in quiet mode (Closes: #932565)
    Many thanks to Antoni Villalonga for reporting the bug and writing a patch.

  * debian/control: Declare compliance with policy v4.4.0
    No change needed

 -- nicoo <nicoo@debian.org>  Tue, 30 Jul 2019 18:38:08 +0200

brightnessctl (0.4-1) unstable; urgency=medium

  [nicoo]
  * New upstream release (2018-09-04)
    + Obey XDG_RUNTIME_DIR for temporary directory

  * Update for Debian Policy v4.3.0
    + debian/control: Set Rules-Requires-Root to no

  * debian/control: Update maintainer email address  \o/

  * debian/rules
    + Make dh_missing fail the build
    + Stop overriding auto_install

  [Ondřej Nový]
  * debian/copyright: Use https protocol in Format field

 -- nicoo <nicoo@debian.org>  Thu, 07 Feb 2019 20:51:07 +0100

brightnessctl (0.3.2-1) unstable; urgency=medium

  * New upstream version (2018-01-24)
    The only changes are the inclusion of the manpage upstream,
    along with manpage improvements credited to Drew DeVault.

  * debian/control:
    - Declare compliance with policy version 4.1.4.
      No change required.
    - Use a canonical Vcs-Git URI

  * debian/rules: Drop a superfluous dh argument

 -- nicoo <nicoo@debian.org>  Mon, 30 Apr 2018 14:08:43 +0200

brightnessctl (0.3.1-1) unstable; urgency=medium

  * New upstream version (2018-01-19)
    * Allow specifying device name as a wildcard
    * Remove Debian patches merged upstream

  * Add brightnessctl(1) manpage
  * Declare compliance with policy version 4.1.3
    No change required
  * Move packaging repository to salsa.d.o
  * Switch to debhelper 11

 -- nicoo <nicoo@debian.org>  Mon, 22 Jan 2018 21:18:41 +0100

brightnessctl (0.3-1) unstable; urgency=low

  * Initial release. (Closes: #880006)

 -- nicoo <nicoo@debian.org>  Sat, 28 Oct 2017 09:59:41 +0200
